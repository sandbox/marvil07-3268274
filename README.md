# Using Migrate API

This is a documentation project intended to help users of Migrate API to arrive
to relevant resources and projects.

The diagrams created based on [Graphviz](https://www.graphviz.org/), please
install it if you want to generate them.

## Diagrams

The following diagrams are available.

- Use Drupal Migrate API. Images: [svg](migrate.svg), [png](migrate.png).
- Migrate into Drupal. Images: [svg](migrate_from_x_to_drupal.svg), [png](migrate_from_x_to_drupal.png).
- Migrate from older Drupal versions. Images: [svg](migrate_older_drupal.svg), [png](migrate_older_drupal.png).
