#!/bin/sh

# Generate svg output.
dot -Tsvg -o migrate.svg migrate.dot
dot -Tsvg -o migrate_from_x_to_drupal.svg migrate_from_x_to_drupal.dot
dot -Tsvg -o migrate_older_drupal.svg migrate_older_drupal.dot

# Generate png output.
dot -Tpng -o migrate.png migrate.dot
dot -Tpng -o migrate_from_x_to_drupal.png migrate_from_x_to_drupal.dot
dot -Tpng -o migrate_older_drupal.png migrate_older_drupal.dot
